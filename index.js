//Version 1

function reverseString(str){
    let newString = '';
    str = str.split('');
    for(let i=str.length-1; i>=0; i--){
        newString += str[i];
    }
    return newString;
}

//console.log(reverseString("Fernand"));

//Version 2

function reverseStringV2(str){
    return str.split('').reverse().join('');
}

//console.log(reverseStringV2("Mario"));


//Count Characters

function countCharacters(str) {
    return str.length;
}

//console.log(countCharacters("Fernand"));


//Capitalize Words

function capitalizeWords(str){
    let firstCharacter = str[0].toUpperCase();
    let newString = firstCharacter + str.slice(1);
    return newString;
}

//console.log(capitalizeWords("Dragon"));



//Find Maximum and Minimum

function minAndMax(arr){
    max = Math.max(...arr);
    min = Math.min(...arr);
    return "min: "+min+" max: "+max; 
}

//console.log(minAndMax([2,9,2,1200,345,123]));



//Sum of Array

function sumArray(arr){
    let sum = 0;
    arr.forEach(element => {
        sum+=element;
    });
    return sum;
}
//console.log(sumArray([1,2,4,244,5324,10,0,0]));



//Filter array

//Methode sans filter()
function filter_array(arr,condition){
    const result = [];
    for (let i = 0; i < arr.length; i++) {
        if(arr[i] == condition){
        result.push(arr[i]);
        }
    }

    return result;
}
const arr = [2,4,1,7,3,1];
//console.log(filter_array(arr,1));



//Methode avec filter()
function filter_array2(arr,condition){
    return arr.filter(function(element){
        return condition(element,value)
    });
}
const arr2 = [2,4,1,7,3,1];
//console.log(filter_array(arr2,4));




//Factorial
function factorialize(num) {
    var result = num;
    if (num === 0 || num === 1) 
        return 1; 
    while (num > 1) { 
        num--;
      result *= num;
    }
    return result;
}

//console.log(factorialize(4));


//Prime number
function prime_number(num){
    let isPrime = true;
    if(num === 1){
        return isPrime;
    }else if(num > 1){
        for(let i = 2; i<num;i++){
            if(num % i == 0){
                isPrime = false;
                break;
            }
        }
    }
    return isPrime;
}

//console.log(prime_number(7));


//Fibonacci_sequence
function fibonacciSequence(n) {
    if (n <= 0) {
        return [];
    } else if (n === 1) {
        return [0];
    } else if (n === 2) {
        return [0, 1];
    }

    const fibSeq = [0, 1];
    for (let i = 2; i < n; i++) {
        const nextTerm = fibSeq[i - 1] + fibSeq[i - 2];
        fibSeq.push(nextTerm);
    }

    return fibSeq;
}

//const sequence = fibonacciSequence(numTerms);
//console.log(sequence);



